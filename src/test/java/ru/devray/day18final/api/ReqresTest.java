package ru.devray.day18final.api;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalTo;

public class ReqresTest {

    //RequestSpecification ResponseSpecification
        ResponseSpecification responseSpecification = new ResponseSpecBuilder()
                .log(LogDetail.ALL)
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .build();
        RequestSpecBuilder specBuilder = new RequestSpecBuilder();
        RequestSpecification requestSpecification = specBuilder
                .setBaseUri("")
                .log(LogDetail.ALL).build();

    @Test
    public void test() {
        //BDD given() when() then()
        RestAssured
                .given()
                .log().all()
                .baseUri("https://reqres.in/api/users/2")
                .queryParam("user", "Alex")
                .when()
                .get() //
                .then()
                .spec(responseSpecification);
        //PathParameters & QueryParameters
        //QP https://reqres.in/api/users?user=ALex&category=sport&price=low
    }


}
