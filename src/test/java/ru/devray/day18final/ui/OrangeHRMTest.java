package ru.devray.day18final.ui;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import java.util.Map;

import static com.codeborne.selenide.Selenide.*;

public class OrangeHRMTest {

    SelenideElement userNameField = $x("//input[@name='username']");
    SelenideElement passwordField = $x("//input[@name='password']");
    SelenideElement loginButton = $x("//button[text()=' Login ']");
    SelenideElement menuSectionRecruitment = $x("//span[text()='Recruitment']");
    SelenideElement firstCandidateTableEntryCheckbox = $x("//div[@class='oxd-table-body']//div[@role='row']//input[1]/following-sibling::span");
    SelenideElement candidateFilterButton = $x("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[2]/div[3]/div/div[1]/div/div[3]/div/i");

    @Test
    public void test2() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("selenoid:options", Map.of("enableVNC", true));
        //PageLoadStrategy EAGER
//        Configuration.remote = "http://51.250.123.179:4444/wd/hub";
//        Configuration.browserSize = "1920x1080";
        Configuration.browser = "chrome";
        Configuration.browserVersion = "112.0";
        Configuration.browserCapabilities = capabilities;

        //Selenide
        open("https://opensource-demo.orangehrmlive.com/");

        //env variables
//        userNameField.sendKeys("Admin");
//        passwordField.sendKeys("admin123");
        userNameField.sendKeys(System.getenv("UI_LOGIN"));
        passwordField.sendKeys(System.getenv("UI_PASS"));

        loginButton.click();
        menuSectionRecruitment.click();
        firstCandidateTableEntryCheckbox.click();
        candidateFilterButton.click();
        firstCandidateTableEntryCheckbox.click();

        sleep(3000);
    }
}
